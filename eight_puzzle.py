#!/usr/bin/env python
# -*- coding: utf-8 -*-

from copy import deepcopy
from sys import argv

START = [
        [4,2,3],
        [8,1,5],
        [7,6,9]
        ]

GOAL  = [
        [1,2,3],
        [4,5,6],
        [7,8,9]
        ]

ADJACENT = (
        (
            ((0,1), (1,0)), # puzzle[0][0] ->
            ((0,0), (0,2), (1,1)), # puzzle[0][1] ->
            ((0,1), (1,2)) # puzzle[0][2] ->
            ),
        (
            ((0,0), (1,1), (2,0)), # puzzle[1][0] ->
            ((0,1), (1,0), (1,2), (2,1)), # puzzle[1][1] ->
            ((0,2), (1,1), (2,2)) # puzzle[1][2] ->
            ),
        (
            ((1,0), (2,1)), # puzzle[2][0] ->
            ((1,1), (2,0), (2,2)), # puzzle[2][1] ->
            ((1,2), (2,1)) # puzzle[2][2] ->
            )
        )

class Puzzle:
    def __init__(self, board, count, prev):
        self.board = board
        self.count = count
        self.pos = self.find_pos() 
        self.prev = prev
    def find_pos(self):
        for c in range(3):
            for n in range(3):
                if self.board[c][n] == 9:
                    return (c, n)

class Puzzle1(Puzzle):
    def __init__(self, board, count, prev):
        super().__init__(board, count, prev)

    def calc_score(self):
        self.score = self.count + self.count_mismatched()
        return self.score

    def make_succ(self):
        succ_board = []
        for x in ADJACENT[self.pos[0]][self.pos[1]]:
            b = deepcopy(self.board)
            b[self.pos[0]][self.pos[1]] = b[x[0]][x[1]]
            b[x[0]][x[1]] = 9
            succ_board.append(b)
        return [Puzzle1(b, self.count + 1, self) for b in succ_board]

    def count_mismatched(self):
        cnt = 0
        for i in range(3):
            for j in range(3):
                panel = self.board[i][j]
                correct = GOAL[i][j]
                if panel != correct and panel != 9:
                    cnt += 1
        return cnt

class Puzzle2(Puzzle):
    def __init__(self, board, count, prev):
        super().__init__(board, count, prev)

    def calc_score(self):
        self.score = self.count + self.measure_mdis()
        return self.score

    def make_succ(self):
        succ_board = []
        for x in ADJACENT[self.pos[0]][self.pos[1]]:
            b = deepcopy(self.board)
            b[self.pos[0]][self.pos[1]] = b[x[0]][x[1]]
            b[x[0]][x[1]] = 9
            succ_board.append(b)
        return [Puzzle2(b, self.count + 1, self) for b in succ_board]

    def measure_mdis(self):
        distance = 0
        for i in range(3):
            for j in range(3):
                panel = self.board[i][j]
                if panel != 9:
                    panel_pos = [i, j]
                    correct_pos = [(panel - 1) // 3, (panel - 1) % 3]
                    distance += abs(panel_pos[0] - correct_pos[0]) + abs(panel_pos[1] - correct_pos[1])
        return distance

def search_astar(cls):
    s = cls(START, 0, None)
    open_list = []
    closed_list = []

    s.calc_score()
    open_list.append(s)

    while len(open_list) != 0:
        open_list.sort(key=lambda p: p.score) # スコアの低い順にソート
        n = open_list.pop(0) # 一番スコアの低いものを取り出す
        closed_list.append(n)

        print("-----")
        print("n:", n.score, n.board)

        if n.board == GOAL:
            print("=====")
            print_ans(n, n.count) # 答えを出力
            return

        for nsucc in n.make_succ():
            nsucc.calc_score()

            print("-----")
            print("succ(n):", nsucc.score, nsucc.board)

            exnsucc = [p for p in open_list + closed_list if p.board == nsucc.board] 
            # open_listかclosed_listの中にnsuccと同じ盤面があるかを見る

            if len(exnsucc) != 0:
                exnsucc = exnsucc[0]
                if exnsucc in open_list:
                    if nsucc.score < exnsucc.score:
                        open_list.append(nsucc)
                        open_list.remove(exnsucc)
                else:
                    if nsucc.score < exnsucc.score:
                        open_list.append(nsucc)
                        closed_list.remove(exnsucc)
            else:
                open_list.append(nsucc)

def search_idastar(cls):
    s = cls(START, 0, None)
    open_list = []
    cutoff = 0
    while True:
        cutoff += 1

        print("#####")
        print("cutoff:", cutoff)

        s.calc_score()
        open_list.append(s)

        while len(open_list) != 0:
            open_list.sort(key=lambda p: p.score) # スコアの低い順にソート
            n = open_list.pop(0) # 最もスコアが小さい節点を取り出す

            print("-----")
            print("n:", n.score, n.board)

            if n.board == GOAL:
                print("=====")
                print_ans(n, n.count) # 答えを出力
                return

            nsuccs = n.make_succ() 
            for ns in nsuccs:
                ns.calc_score()
                print("-----")
                print("succ(n):", ns.score, ns.board)

            nsuccs = [ns for ns in nsuccs if ns.score <= cutoff] 
            # scoreがcutoff以下のnsuccsに対して実行
            for nsucc in nsuccs:
                open_list.append(nsucc)

def print_ans(x, c):
    if x is not None:
        print_ans(x.prev, c-1)
        print(c, x.score, x.board)


if __name__ == '__main__':
    assert len(argv) >= 2, '引数が1つ必要です'
    if argv[1] == str(0):
        search_astar(Puzzle1)
    if argv[1] == str(1):
        search_astar(Puzzle2)
    if argv[1] == str(2):
        search_idastar(Puzzle1)
    if argv[1] == str(3):
        search_idastar(Puzzle2)


